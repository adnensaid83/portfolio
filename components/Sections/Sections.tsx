import Hero from '../Home/Hero';
import Testimonials from '../Home/Testimonials';
import Video from '../Home/Video';
import DetailSection from '../Home/Detail/DetailSection';
import {
  AboutIntroRecord,
  AllProjectsSectionRecord,
  CollectionMetadata,
  ContactRecord,
  ContactSectionRecord,
  DetailSectionRecord,
  GoToContactRecord,
  HeroSectionPageRecord,
  HeroSectionRecord,
  HowItWorkRecord,
  PageModelSectionsField,
  PostRecord,
  PricingSectionRecord,
  ProjectSectionRecord,
  RedirectSectionRecord,
  ReviewSectionRecord,
  ServiceSectionRecord,
  SiteLocale,
  VideoSectionRecord,
} from '@/graphql/types/graphql';
import { redirect } from 'next/navigation';
import Carrousel from '../Home/Testimonials/Carrousel';
import ModernCarrousel from '../Home/Testimonials/ModernCarrousel';
import MinimalCarrousel from '../Home/Testimonials/MinimalCarrousel';
import MinimalReviewCards from '../Home/Testimonials/MinimalReviewCards';
import PortraitImage from '../Home/Hero/PortraitImage';
import Service from '../Home/Service';
import Project from '../Home/Projects';
import Contact from '../Home/Contact';
import AllProject from '../Portfolio/Projects';
import ContactPage from '../Contact';
import HeroSectionPage from '../Home/Hero/HeroSectionPage';
import HowItWork from '../Service/HowItWork';
import GoToContact from '../Contact/GoToContact';
import AboutIntro from '../About/AboutIntro';

type Props = {
  sections: Array<PageModelSectionsField>;
  locale: SiteLocale;
  posts: PostRecord[];
  postMeta: CollectionMetadata;
  token: string | undefined;
  quoteRequestId: string | undefined;
};

export default function Section({
  sections,
  locale,
  token,
  quoteRequestId,
}: Props) {
  return (
    <>
      {sections.map((section) => {
        switch (section._modelApiKey) {
          case 'go_to_contact':
            const goToContactRecord = section as GoToContactRecord;
            return (
              <GoToContact
                key={goToContactRecord.id}
                title={goToContactRecord.title}
                subtitle={goToContactRecord.subtitle}
                button={goToContactRecord.button}
              />
            );
          case 'how_it_work':
            const howItWorkRecord = section as HowItWorkRecord;
            return (
              <HowItWork
                key={howItWorkRecord.id}
                title={howItWorkRecord.title}
                description={howItWorkRecord.description}
                step={howItWorkRecord.step}
              />
            );
          case 'hero_section_page':
            const heroSectionPage = section as HeroSectionPageRecord;
            return (
              <HeroSectionPage
                key={heroSectionPage.id}
                title={heroSectionPage.title}
                locale={locale}
              />
            );
          case 'contact_section':
            const contactSectionRecord = section as ContactSectionRecord;
            return (
              <ContactPage
                key={contactSectionRecord.id}
                title={contactSectionRecord.title}
                subtitle={contactSectionRecord.subtitle}
                description={contactSectionRecord.description}
                question={contactSectionRecord.question}
                token={token || ''}
                quoteRequestId={quoteRequestId || ''}
                contactInfo={contactSectionRecord.contactInfo}
                projectView={contactSectionRecord.projectView}
                locale={locale}
              />
            );
          case 'all_projects_section':
            const allProjectsSection = section as AllProjectsSectionRecord;
            return (
              <AllProject
                key={allProjectsSection.id}
                title={allProjectsSection.title}
                subtitle={allProjectsSection.subtitle}
                project={allProjectsSection.project}
                locale={locale}
              />
            );
          case 'contact':
            const contactSection = section as ContactRecord;
            return (
              <Contact
                key={contactSection.id}
                title={contactSection.title}
                subtitle={contactSection.subtitle}
                button={contactSection.buttonContact}
              />
            );
          case 'project_section':
            const projectSection = section as ProjectSectionRecord;
            return (
              <Project
                key={projectSection.id}
                title={projectSection.title}
                project={projectSection.project}
                button={projectSection.button}
              />
            );
          case 'service_section':
            const serviceSection = section as ServiceSectionRecord;
            return (
              <Service
                key={serviceSection.id}
                title={serviceSection.title}
                service={serviceSection.service}
                button={serviceSection.buttonService}
                locale={locale}
              />
            );
          case 'hero_section':
            const heroSectionRecord = section as HeroSectionRecord;
            switch (heroSectionRecord.displayOptions) {
              case 'portrait_image':
                return (
                  <PortraitImage
                    key={heroSectionRecord.id}
                    heroTitle={heroSectionRecord.heroTitle}
                    image={heroSectionRecord.heroImage}
                    heroSubtitle={heroSectionRecord.heroSubtitle}
                    buttons={heroSectionRecord.buttons}
                    skillTitle={heroSectionRecord.skillTitle}
                    skill={heroSectionRecord.skill}
                    locale={locale}
                  />
                );
              default:
                return (
                  <Hero
                    key={heroSectionRecord.id}
                    heroTitle={heroSectionRecord.heroTitle}
                    heroSubtitle={heroSectionRecord.heroSubtitle}
                    buttons={heroSectionRecord.buttons}
                  />
                );
            }
          case 'video_section':
            const videoSectionRecord = section as VideoSectionRecord;
            return (
              <Video
                key={videoSectionRecord.id}
                videoHeader={videoSectionRecord.videoHeader}
                videoSubheader={videoSectionRecord.videoSubheader}
                videoUid={videoSectionRecord.video?.providerUid}
                videoThumbnail={videoSectionRecord.videoThumbnail}
                videoProvider={videoSectionRecord.video?.provider}
              />
            );
          case 'detail_section':
            const detailSectionRecord = section as DetailSectionRecord;
            return (
              <DetailSection
                key={detailSectionRecord.id}
                imagePosition={detailSectionRecord.imagePosition as boolean}
                image={detailSectionRecord.image}
                details={detailSectionRecord.details}
              />
            );
          case 'review_section':
            const reviewSectionRecord = section as ReviewSectionRecord;
            switch (reviewSectionRecord.displayOptions) {
              case 'card_carrousel':
                return (
                  <Carrousel
                    key={reviewSectionRecord.id}
                    header={reviewSectionRecord.reviewSectionHeader}
                    subheader={reviewSectionRecord.reviewSectionSubheader}
                    reviews={reviewSectionRecord.reviews}
                  />
                );
              case 'modern_carrousel':
                return (
                  <ModernCarrousel
                    key={reviewSectionRecord.id}
                    header={reviewSectionRecord.reviewSectionHeader}
                    subheader={reviewSectionRecord.reviewSectionSubheader}
                    reviews={reviewSectionRecord.reviews}
                  />
                );
              case 'minimal_carrousel':
                return (
                  <MinimalCarrousel
                    key={reviewSectionRecord.id}
                    header={reviewSectionRecord.reviewSectionHeader}
                    subheader={reviewSectionRecord.reviewSectionSubheader}
                    reviews={reviewSectionRecord.reviews}
                  />
                );
              case 'minimal_cards':
                return (
                  <MinimalReviewCards
                    key={reviewSectionRecord.id}
                    header={reviewSectionRecord.reviewSectionHeader}
                    subheader={reviewSectionRecord.reviewSectionSubheader}
                    reviews={reviewSectionRecord.reviews}
                  />
                );
              default:
                return (
                  <Testimonials
                    key={reviewSectionRecord.id}
                    header={reviewSectionRecord.reviewSectionHeader}
                    subheader={reviewSectionRecord.reviewSectionSubheader}
                    reviews={reviewSectionRecord.reviews}
                  />
                );
            }
          case 'about_intro':
            const aboutIntroSectionRecord = section as AboutIntroRecord;
            return (
              <AboutIntro
                key={aboutIntroSectionRecord.id}
                header={aboutIntroSectionRecord.header || ''}
                subheader={aboutIntroSectionRecord.subheader}
                introduction={aboutIntroSectionRecord.introductionText}
                button={aboutIntroSectionRecord.button}
                images={aboutIntroSectionRecord.images}
                preHeader={aboutIntroSectionRecord.preHeader}
              />
            );
          case 'redirect_section':
            const redirectSectionRecord = section as RedirectSectionRecord;
            redirect(`/${locale}/${redirectSectionRecord.slugToRedirectTo}`);
          default:
            return <></>;
        }
      })}
    </>
  );
}
