import Image from 'next/image';
import Link from 'next/link';
import SvgRenderer from '../Common/SvgRenderer';
import {
  ChangeLogRecord,
  FooterQuery,
  LegalPageRecord,
  SiteLocale,
} from '@/graphql/types/graphql';
import { notFound } from 'next/navigation';
import { primaryColor } from '@/app/i18n/settings';
import ReactMarkdown from 'react-markdown';

type Props = {
  data: FooterQuery;
  lng: SiteLocale;
};

const Footer = ({ data, lng }: Props) => {
  return (
    <footer className="relative z-10 mx-auto flex w-full flex-col items-center justify-center border-t-2 border-dashed border-primary bg-white py-6 text-center md:text-start">
      <div className="container w-full">
        <div className="flex w-full flex-col justify-between md:flex-row">
          <div className="w-full">
            <div className="flex flex-col gap-12 md:items-start lg:mx-12 lg:flex-row lg:items-center lg:justify-between">
              <div className="flex flex-col items-center justify-center md:items-start">
                <div className="w-36">
                  <Link
                    href={'/' + lng + '/home'}
                    className="mb-2 inline-block"
                  >
                    {data.layout?.footerLogo && (
                      <Image
                        src={data.layout.footerLogo.url}
                        alt="logo"
                        className="w-full"
                        width={data.layout.footerLogo.width || 240}
                        height={data.layout.footerLogo.height || 60}
                      />
                    )}
                  </Link>
                </div>
                <div
                  className={`text-xs font-medium leading-relaxed text-black ${
                    lng === 'ar' && 'text-center'
                  }`}
                >
                  <ReactMarkdown>
                    {data.layout!.footerSubtitle || ''}
                  </ReactMarkdown>
                </div>
              </div>
              <ul className="flex flex-col gap-2 md:flex-row">
                <li>
                  <p className="inline-block text-xs font-medium text-black hover:text-primary">
                    ©2024 - Design & Development by Adnen - Made with DatoCMS
                  </p>
                </li>
                {data.layout!.footerLinks.map((link) => {
                  const pageLink = link as LegalPageRecord; // The field has a "at least one" validation
                  return (
                    <li key={pageLink.id}>
                      <a
                        href={'/' + lng + '/legal/' + pageLink.slug}
                        className="inline-block text-xs font-medium text-black hover:text-primary"
                      >
                        {pageLink.title}
                      </a>
                    </li>
                  );
                })}
              </ul>
              <div className="flex items-center justify-center">
                {data.layout!.socialMediaLinks.map((socialMedia) => {
                  return (
                    <a
                      href={socialMedia.url}
                      aria-label="social-link"
                      className="mr-6 text-black hover:text-primary"
                      key={socialMedia.id}
                    >
                      <SvgRenderer url={socialMedia.icon.url} />
                    </a>
                  );
                })}
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
