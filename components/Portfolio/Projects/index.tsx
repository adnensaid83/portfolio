'use client';

import TitleSection from '@/components/ui/TitleSection';
import { ProjectRecord } from '@/graphql/types/graphql';
import React from 'react';
import ProjectItem from './ProjectItem';
import { RegularList } from '@/components/Common/RegularList';
import { Maybe } from 'graphql/jsutils/Maybe';
import ReactMarkdown from 'react-markdown';
import { useScroll, useTransform, motion } from 'framer-motion';
import { primaryColor } from '@/app/i18n/settings';

type Props = {
  title: string;
  subtitle: Maybe<string>;
  project: ProjectRecord[];
  locale?: string;
};
const AllProject = ({ title, project, subtitle, locale }: Props) => {
  return (
    <>
      <div className="mx-auto max-w-[1200px] px-6">
        <div className="prose mx-auto max-w-3xl pt-12 text-center text-base text-black prose-strong:font-normal prose-strong:text-primary sm:text-lg md:text-xl">
          <ReactMarkdown>{subtitle || ''}</ReactMarkdown>
        </div>
        <ul className="grid grid-cols-1 place-items-center gap-6 py-12 md:grid-cols-2 md:gap-16 lg:grid-cols-3 lg:gap-16 ">
          <RegularList
            items={project}
            resourceName="project"
            itemComponent={ProjectItem}
          />
        </ul>
      </div>
    </>
  );
};

export default AllProject;
