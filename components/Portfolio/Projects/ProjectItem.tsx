import { ProjectRecord } from '@/graphql/types/graphql';
import Image from 'next/image';
import Link from 'next/link';
import React from 'react';
import { Image as DatoImage } from 'react-datocms';

const ProjectItem = ({ project }: { project: ProjectRecord }) => {
  const { title, image, web, git, tech } = project;
  return (
    <div className="lg:max-w-auto group relative max-w-[300px] shrink-0 cursor-pointer overflow-hidden rounded-xl md:max-w-[380px]">
      {image && image.responsiveImage && (
        <DatoImage data={image?.responsiveImage} />
      )}
      <Link
        href={web || '#'}
        target="_blank"
        className={`absolute top-0 h-full w-full lg:hidden`}
      />
      <div className="hidden transition duration-300 lg:absolute lg:top-0 lg:h-full lg:w-full lg:flex-col lg:bg-[rgba(0,0,0,.8)] lg:group-hover:flex">
        <h5 className="bg-green py-4 text-center font-medium text-black">
          {title}
        </h5>
        <div className=" flex flex-1 items-center justify-center gap-4">
          {git && (
            <Link href={git} target="_blank">
              <div className="flex h-[36px] w-[36px] items-center justify-center rounded-full border-2 border-white p-[8px] text-white hover:border-white hover:bg-green hover:text-black">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="16"
                  height="16"
                  fill="currentColor"
                  className="bi bi-gitlab"
                  viewBox="0 0 16 16"
                >
                  <path d="m15.734 6.1-.022-.058L13.534.358a.57.57 0 0 0-.563-.356.6.6 0 0 0-.328.122.6.6 0 0 0-.193.294l-1.47 4.499H5.025l-1.47-4.5A.572.572 0 0 0 2.47.358L.289 6.04l-.022.057A4.044 4.044 0 0 0 1.61 10.77l.007.006.02.014 3.318 2.485 1.64 1.242 1 .755a.67.67 0 0 0 .814 0l1-.755 1.64-1.242 3.338-2.5.009-.007a4.05 4.05 0 0 0 1.34-4.668Z" />
                </svg>
              </div>
            </Link>
          )}
          <Link href={web || '#'} target="_blank">
            <div className="flex h-[36px] w-[36px] items-center justify-center rounded-full border-2 border-white p-[8px] text-white hover:border-white hover:bg-green hover:text-black">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="16"
                height="16"
                fill="currentColor"
                className="bi bi-browser-chrome"
                viewBox="0 0 16 16"
              >
                <path
                  fillRule="evenodd"
                  d="M16 8a8 8 0 0 1-7.022 7.94l1.902-7.098a3 3 0 0 0 .05-1.492A3 3 0 0 0 10.237 6h5.511A8 8 0 0 1 16 8M0 8a8 8 0 0 0 7.927 8l1.426-5.321a3 3 0 0 1-.723.255 3 3 0 0 1-1.743-.147 3 3 0 0 1-1.043-.7L.633 4.876A8 8 0 0 0 0 8m5.004-.167L1.108 3.936A8.003 8.003 0 0 1 15.418 5H8.066a3 3 0 0 0-1.252.243 2.99 2.99 0 0 0-1.81 2.59M8 10a2 2 0 1 0 0-4 2 2 0 0 0 0 4"
                />
              </svg>
            </div>
          </Link>
        </div>
        <div className="flex flex-wrap items-center justify-center gap-2 pb-4">
          {tech.map((s, i) => (
            <Image
              key={i}
              src={s.url}
              alt={s.label}
              className="z-30"
              width={44}
              height={44}
              priority
            />
          ))}
        </div>
      </div>
    </div>
  );
};

export default ProjectItem;
