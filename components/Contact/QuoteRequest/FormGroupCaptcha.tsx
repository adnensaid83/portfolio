import ExclamationCircle from './ExclamationCircle';
import { FormGroupProps } from './FormGroup';
import QuestionLabel from './QuestionLabel';
import StepNumber from './StepNumber';

export const FormGroupCaptcha = ({
  children,
  errorMessage,
  helper,
  label,
  order,
  showError,
}: FormGroupProps) => (
  <div className="relative">
    <QuestionLabel step={order || 0} label={label} />
    <div className={`mb-4`}>{children}</div>
    {!!helper && <span>{helper}</span>}
    {!!errorMessage && showError && (
      <div className="absolute left-24 flex items-center gap-2 text-xs text-red-500">
        <div className="w-6">
          <ExclamationCircle />
        </div>
        {errorMessage}
      </div>
    )}
  </div>
);
