import { FieldProps, useField } from "@formiz/core";
import { FormGroupProps } from "./FormGroup";
import Spin from "./spin";

type Value = string;

export type FieldInputProps<FormattedValue = Value> = FieldProps<
  Value,
  FormattedValue
> &
  FormGroupProps & {
    isLoading?: boolean;
  };

export const FieldTextarea = <FormattedValue = Value,>(
  props: FieldInputProps<FormattedValue>
) => {
  const {
    id,
    isValid,
    isTouched,
    isRequired,
    isValidating,
    isProcessing,
    isDebouncing,
    isReady,
    isPristine,
    setValue,
    value,
    setIsTouched,
    otherProps: {
      children,
      label,
      placeholder,
      autoFocus = false,
      isLoading = false,
    },
  } = useField(props);

  return (
    <div className="col-span-1 md:col-span-2">
      {/*       <label
        htmlFor={id}
        className="mb-4 block text-base font-medium text-dark"
      >
        {label} {isRequired && "*"}
      </label> */}
      <textarea
        className={`mb-0 w-full rounded-xl border-2 border-dark p-2 text-base ${
          !isValid && isTouched && "border-accent"
        } ${!isProcessing && isValid && "border-green200"}`}
        id={id}
        rows={5}
        value={value ?? ""}
        onChange={(e) => setValue(e.target.value)}
        onFocus={() => setIsTouched(false)}
        onBlur={() => setIsTouched(true)}
        placeholder={placeholder}
        autoFocus={autoFocus}
      />
      <div>
        {!isReady && "🚧"}
        {isReady && isProcessing && "⏳"}
        {/* {!isValid && isTouched && "❌"} */}
        {/* {!isProcessing && isValid && "✅"} */}
      </div>
      <div>
        {isDebouncing && "✋🏼"}
        {isValidating || isLoading ? <Spin /> : isPristine && ""}
      </div>
      {children}
    </div>
  );
};
