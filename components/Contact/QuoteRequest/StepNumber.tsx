import React from 'react';

const StepNumber = ({ step }: { step: number }) => {
  return (
    <span className="flex h-10 w-10 shrink-0 items-center justify-center rounded-full border-2 border-primary bg-primary text-xl font-semibold text-white md:h-12 md:w-12 md:text-2xl">
      {step}
    </span>
  );
};

export default StepNumber;
