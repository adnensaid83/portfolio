import React from 'react';
import StepNumber from './StepNumber';

const QuestionLabel = ({ label, step }: { label: any; step: number }) => {
  return (
    <div className="mb-6 flex items-center gap-4">
      <StepNumber step={step} />
      <p className="mb-0 text-xl font-semibold lowercase text-black md:text-2xl xl:text-3xl">
        {label}
      </p>
    </div>
  );
};

export default QuestionLabel;
