import React from 'react';
import { ButtonRecord } from '@/graphql/types/graphql';
import ButtonPrimary from '../ui/ButtonPrimary';
import { Maybe } from 'graphql/jsutils/Maybe';

type Props = {
  title: string;
  subtitle: Maybe<string>;
  button: Maybe<ButtonRecord>;
};
const GoToContact = ({ title, subtitle, button }: Props) => {
  return (
    <div className="bg-green py-12 xl:py-24">
      <div className="mx-auto max-w-4xl px-2">
        <h4 className="mb-6 text-center text-3xl font-semibold prose-strong:text-primary md:mb-8 md:text-4xl xl:mb-12 xl:text-5xl">
          {title}
        </h4>
        <p className="mb-6 text-center text-xl md:mb-8 md:text-2xl xl:mb-12 xl:text-3xl">
          {subtitle}
        </p>
        <ButtonPrimary url={button?.url || ''} label={button?.label || ''} />
      </div>
    </div>
  );
};

export default GoToContact;
