'use client';
import { Maybe } from 'graphql/jsutils/Maybe';
import Link from 'next/link';
import React from 'react';
import { primaryColor } from '@/app/i18n/settings';
import { useScroll, useTransform, motion } from 'framer-motion';
import ReactMarkdown from 'react-markdown';
import {
  ContactInfoRecord,
  ContactRecord,
  QuestionContactRecord,
} from '@/graphql/types/graphql';
import QuoteRequest from './QuoteRequest/QuoteRequest';
import ButtonPrimary from '../ui/ButtonPrimary';
type Props = {
  title: string;
  subtitle: Maybe<string>;
  description: Maybe<string>;
  question: QuestionContactRecord[];
  token: string;
  quoteRequestId: string;
  contactInfo: Maybe<ContactInfoRecord>;
  projectView: Maybe<ContactRecord>;
  locale?: string;
};
const ContactPage = ({
  title,
  subtitle,
  description,
  question,
  token,
  quoteRequestId,
  contactInfo,
  projectView,
  locale,
}: Props) => {
  const { scrollYProgress } = useScroll();
  const y = useTransform(scrollYProgress, [0, 1], ['0%', '500%']);
  return (
    <>
      <div className="relative z-10 overflow-hidden bg-green py-32">
        <div className="container">
          <div className="-mx-4 flex flex-wrap">
            <div className="w-full px-4">
              <div className="mx-auto max-w-[930px] p-[1rem]">
                <h1 className="mb-6 text-4xl font-bold text-primary md:mb-8 md:text-5xl xl:mb-12 xl:text-6xl">
                  {title}
                </h1>
                <div className="text-primary">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="42"
                    height="62"
                    fill="currentColor"
                    className="bi bi-arrow-down rounded-full border-2 border-primary px-2 py-4"
                    viewBox="0 0 16 16"
                  >
                    <path
                      fillRule="evenodd"
                      d="M8 1a.5.5 0 0 1 .5.5v11.793l3.146-3.147a.5.5 0 0 1 .708.708l-4 4a.5.5 0 0 1-.708 0l-4-4a.5.5 0 0 1 .708-.708L7.5 13.293V1.5A.5.5 0 0 1 8 1"
                    />
                  </svg>
                </div>
              </div>
            </div>
          </div>
        </div>
        <motion.div
          initial={{ opacity: 0 }}
          animate={{ opacity: 1 }}
          transition={{ duration: 0.5, delay: 0.1 }}
          /* style={{ y }} */
          className={`absolute ${
            locale === 'ar' ? 'left-0' : 'right-0'
          }  top-0 z-[-1] opacity-30 lg:opacity-100`}
        >
          <svg
            width="450"
            height="556"
            viewBox="0 0 450 556"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            className={`rotate-90 md:rotate-0 ${
              locale === 'ar' && 'lg:-rotate-90'
            }`}
          >
            <circle
              cx="277"
              cy="63"
              r="225"
              fill="url(#paint0_linear_25:217)"
            />
            <circle
              cx="17.9997"
              cy="182"
              r="18"
              fill="url(#paint1_radial_25:217)"
            />
            <circle
              cx="76.9997"
              cy="288"
              r="34"
              fill="url(#paint2_radial_25:217)"
            />
            <circle
              cx="325.486"
              cy="302.87"
              r="180"
              transform="rotate(-37.6852 325.486 302.87)"
              fill="url(#paint3_linear_25:217)"
            />
            <circle
              opacity="0.8"
              cx="184.521"
              cy="315.521"
              r="132.862"
              transform="rotate(114.874 184.521 315.521)"
              stroke="url(#paint4_linear_25:217)"
            />
            <circle
              opacity="0.8"
              cx="356"
              cy="290"
              r="179.5"
              transform="rotate(-30 356 290)"
              stroke="url(#paint5_linear_25:217)"
            />
            <circle
              opacity="0.8"
              cx="191.659"
              cy="302.659"
              r="133.362"
              transform="rotate(133.319 191.659 302.659)"
              fill="url(#paint6_linear_25:217)"
            />
            <defs>
              <linearGradient
                id="paint0_linear_25:217"
                x1="-54.5003"
                y1="-178"
                x2="222"
                y2="288"
                gradientUnits="userSpaceOnUse"
              >
                <stop stopColor={primaryColor} />
                <stop offset="1" stopColor={primaryColor} stopOpacity="0" />
              </linearGradient>
              <radialGradient
                id="paint1_radial_25:217"
                cx="0"
                cy="0"
                r="1"
                gradientUnits="userSpaceOnUse"
                gradientTransform="translate(17.9997 182) rotate(90) scale(18)"
              >
                <stop
                  offset="0.145833"
                  stopColor={primaryColor}
                  stopOpacity="0"
                />
                <stop offset="1" stopColor={primaryColor} stopOpacity="0.08" />
              </radialGradient>
              <radialGradient
                id="paint2_radial_25:217"
                cx="0"
                cy="0"
                r="1"
                gradientUnits="userSpaceOnUse"
                gradientTransform="translate(76.9997 288) rotate(90) scale(34)"
              >
                <stop
                  offset="0.145833"
                  stopColor={primaryColor}
                  stopOpacity="0"
                />
                <stop offset="1" stopColor={primaryColor} stopOpacity="0.08" />
              </radialGradient>
              <linearGradient
                id="paint3_linear_25:217"
                x1="226.775"
                y1="-66.1548"
                x2="292.157"
                y2="351.421"
                gradientUnits="userSpaceOnUse"
              >
                <stop stopColor={primaryColor} />
                <stop offset="1" stopColor={primaryColor} stopOpacity="0" />
              </linearGradient>
              <linearGradient
                id="paint4_linear_25:217"
                x1="184.521"
                y1="182.159"
                x2="184.521"
                y2="448.882"
                gradientUnits="userSpaceOnUse"
              >
                <stop stopColor={primaryColor} />
                <stop offset="1" stopColor="white" stopOpacity="0" />
              </linearGradient>
              <linearGradient
                id="paint5_linear_25:217"
                x1="356"
                y1="110"
                x2="356"
                y2="470"
                gradientUnits="userSpaceOnUse"
              >
                <stop stopColor={primaryColor} />
                <stop offset="1" stopColor="white" stopOpacity="0" />
              </linearGradient>
              <linearGradient
                id="paint6_linear_25:217"
                x1="118.524"
                y1="29.2497"
                x2="166.965"
                y2="338.63"
                gradientUnits="userSpaceOnUse"
              >
                <stop stopColor={primaryColor} />
                <stop offset="1" stopColor={primaryColor} stopOpacity="0" />
              </linearGradient>
            </defs>
          </svg>
        </motion.div>
        <motion.div
          /* style={{ y }} */
          initial={{ opacity: 0 }}
          animate={{ opacity: 1 }}
          transition={{ duration: 0.5, delay: 0.1 }}
          className={`absolute bottom-0 ${
            locale === 'ar' ? 'right-0' : 'left-0'
          } z-[-1] opacity-30 lg:opacity-100`}
        >
          <svg
            width="364"
            height="201"
            viewBox="0 0 364 201"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              d="M5.88928 72.3303C33.6599 66.4798 101.397 64.9086 150.178 105.427C211.155 156.076 229.59 162.093 264.333 166.607C299.076 171.12 337.718 183.657 362.889 212.24"
              stroke="url(#paint0_linear_25:218)"
            />
            <path
              d="M-22.1107 72.3303C5.65989 66.4798 73.3965 64.9086 122.178 105.427C183.155 156.076 201.59 162.093 236.333 166.607C271.076 171.12 309.718 183.657 334.889 212.24"
              stroke="url(#paint1_linear_25:218)"
            />
            <path
              d="M-53.1107 72.3303C-25.3401 66.4798 42.3965 64.9086 91.1783 105.427C152.155 156.076 170.59 162.093 205.333 166.607C240.076 171.12 278.718 183.657 303.889 212.24"
              stroke="url(#paint2_linear_25:218)"
            />
            <path
              d="M-98.1618 65.0889C-68.1416 60.0601 4.73364 60.4882 56.0734 102.431C120.248 154.86 139.905 161.419 177.137 166.956C214.37 172.493 255.575 186.165 281.856 215.481"
              stroke="url(#paint3_linear_25:218)"
            />
            <circle
              opacity="0.8"
              cx="214.505"
              cy="60.5054"
              r="49.7205"
              transform="rotate(-13.421 214.505 60.5054)"
              stroke="url(#paint4_linear_25:218)"
            />
            <circle cx="220" cy="63" r="43" fill="url(#paint5_radial_25:218)" />
            <defs>
              <linearGradient
                id="paint0_linear_25:218"
                x1="184.389"
                y1="69.2405"
                x2="184.389"
                y2="212.24"
                gradientUnits="userSpaceOnUse"
              >
                <stop stopColor={primaryColor} stopOpacity="0" />
                <stop offset="1" stopColor={primaryColor} />
              </linearGradient>
              <linearGradient
                id="paint1_linear_25:218"
                x1="156.389"
                y1="69.2405"
                x2="156.389"
                y2="212.24"
                gradientUnits="userSpaceOnUse"
              >
                <stop stopColor={primaryColor} stopOpacity="0" />
                <stop offset="1" stopColor={primaryColor} />
              </linearGradient>
              <linearGradient
                id="paint2_linear_25:218"
                x1="125.389"
                y1="69.2405"
                x2="125.389"
                y2="212.24"
                gradientUnits="userSpaceOnUse"
              >
                <stop stopColor={primaryColor} stopOpacity="0" />
                <stop offset="1" stopColor={primaryColor} />
              </linearGradient>
              <linearGradient
                id="paint3_linear_25:218"
                x1="93.8507"
                y1="67.2674"
                x2="89.9278"
                y2="210.214"
                gradientUnits="userSpaceOnUse"
              >
                <stop stopColor={primaryColor} stopOpacity="0" />
                <stop offset="1" stopColor={primaryColor} />
              </linearGradient>
              <linearGradient
                id="paint4_linear_25:218"
                x1="214.505"
                y1="10.2849"
                x2="212.684"
                y2="99.5816"
                gradientUnits="userSpaceOnUse"
              >
                <stop stopColor={primaryColor} />
                <stop offset="1" stopColor={primaryColor} stopOpacity="0" />
              </linearGradient>
              <radialGradient
                id="paint5_radial_25:218"
                cx="0"
                cy="0"
                r="1"
                gradientUnits="userSpaceOnUse"
                gradientTransform="translate(220 63) rotate(90) scale(43)"
              >
                <stop offset="0.145833" stopColor="white" stopOpacity="0" />
                <stop offset="1" stopColor="white" stopOpacity="0.08" />
              </radialGradient>
            </defs>
          </svg>
        </motion.div>
      </div>
      <div className="mx-auto max-w-4xl px-4">
        <div className="py-12 xl:py-24">
          <h2 className="mb-6 text-3xl font-semibold prose-strong:text-primary md:mb-8 md:text-3xl xl:mb-12 xl:text-5xl">
            <ReactMarkdown>{subtitle || ''}</ReactMarkdown>
          </h2>
          <div className="mb-6 text-lg prose-strong:text-primary md:mb-8 md:text-xl xl:mb-12 xl:text-2xl">
            <ReactMarkdown>{description || ''}</ReactMarkdown>
          </div>
          <QuoteRequest
            service={question}
            token={token}
            id={quoteRequestId}
            locale={locale}
          />
        </div>
      </div>
      <div className="mx-auto max-w-4xl px-4 pb-12 xl:pb-24">
        <h3 className="mb-6 text-3xl font-semibold prose-strong:text-primary md:mb-8 md:text-3xl xl:mb-12 xl:text-5xl">
          {contactInfo?.contactInfoLabel}
        </h3>
        <div className="flex flex-col gap-8 md:flex-row md:justify-between md:gap-0">
          <div>
            <p className="mb-2"> {contactInfo?.localityLabel} </p>
            <p className="max-w-xs text-xl md:text-2xl">
              {contactInfo?.localityValue}
            </p>
          </div>
          <div>
            <div className="mb-6">
              <p className="mb-2"> {contactInfo?.emailLabel} </p>
              <p className="max-w-xs text-xl">{contactInfo?.emailValue}</p>
            </div>
            <div className="mb-6">
              <p className="mb-2"> {contactInfo?.phoneLabel} </p>
              <p className="max-w-xs text-xl">{contactInfo?.phoneValue}</p>
            </div>
            <div className="mb-6">
              <p className="mb-2"> {contactInfo?.socialMediaLabel} </p>
              <div className="flex max-w-xs gap-2 text-xl">
                <Link
                  href={'https://www.linkedin.com/in/adnensaid83/'}
                  className="hover:text-primary"
                >
                  Linkedin
                </Link>
                <Link
                  href={'https://medium.com/@adnensaid83'}
                  className="hover:text-primary"
                >
                  Medium
                </Link>
                <Link
                  href={'https://gitlab.com/adnensaid83'}
                  className="hover:text-primary"
                >
                  Gitlab
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="bg-green py-12 xl:py-24">
        <div className="mx-auto max-w-xl">
          <h4 className="mb-6 text-center text-3xl font-semibold prose-strong:text-primary md:mb-8 md:text-4xl xl:mb-12 xl:text-5xl">
            {projectView?.title}
          </h4>
          <p className="mb-6 text-center text-xl md:mb-8 md:text-2xl xl:mb-12 xl:text-3xl">
            {projectView?.subtitle}
          </p>
          <ButtonPrimary
            url={projectView?.buttonContact.url || ''}
            label={projectView?.buttonContact.label || ''}
          />
        </div>
      </div>
    </>
  );
};

export default ContactPage;

type ContactItemProps = {
  icon: any;
  label: string;
  value: string;
};
function ContactItem({ icon, label, value }: ContactItemProps) {
  return (
    <div className="flex gap-4">
      <div className="text-green200 h-[48px] w-[48px] rounded-full border-2 border-white bg-white p-4 drop-shadow-md md:h-[60px] md:w-[60px]">
        {icon}
      </div>
      <div>
        <h3 className="text-l font-bold md:text-xl"> {label} </h3>
        <address className="text-l text-gray100 text-center font-light not-italic leading-none md:text-xl">
          <a href="mailto:web@adnensaid.fr"> {value} </a>
        </address>
      </div>
    </div>
  );
}

export function GeoIcon() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="100%"
      height="100%"
      fill="currentColor"
      className="bi bi-geo-alt"
      viewBox="0 0 16 16"
    >
      <path d="M12.166 8.94c-.524 1.062-1.234 2.12-1.96 3.07A31.493 31.493 0 0 1 8 14.58a31.481 31.481 0 0 1-2.206-2.57c-.726-.95-1.436-2.008-1.96-3.07C3.304 7.867 3 6.862 3 6a5 5 0 0 1 10 0c0 .862-.305 1.867-.834 2.94M8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10" />
      <path d="M8 8a2 2 0 1 1 0-4 2 2 0 0 1 0 4m0 1a3 3 0 1 0 0-6 3 3 0 0 0 0 6" />
    </svg>
  );
}
export function MailIcon() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="100%"
      height="100%"
      fill="currentColor"
      className="bi bi-envelope"
      viewBox="0 0 16 16"
    >
      <path d="M0 4a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2zm2-1a1 1 0 0 0-1 1v.217l7 4.2 7-4.2V4a1 1 0 0 0-1-1zm13 2.383-4.708 2.825L15 11.105zm-.034 6.876-5.64-3.471L8 9.583l-1.326-.795-5.64 3.47A1 1 0 0 0 2 13h12a1 1 0 0 0 .966-.741M1 11.105l4.708-2.897L1 5.383z" />
    </svg>
  );
}

export function PhoneIcon() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="100%"
      height="100%"
      fill="currentColor"
      className="bi bi-phone"
      viewBox="0 0 16 16"
    >
      <path d="M11 1a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1zM5 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2z" />
      <path d="M8 14a1 1 0 1 0 0-2 1 1 0 0 0 0 2" />
    </svg>
  );
}

/* 
          

*/
