import { StepWorkRecord } from '@/graphql/types/graphql';
import { Maybe } from 'graphql/jsutils/Maybe';
import React from 'react';

type Props = {
  title: string;
  description: Maybe<string>;
  step: StepWorkRecord[];
};
const HowItWork = ({ title, description, step }: Props) => {
  return (
    <div className="mb-16 bg-green py-12 md:mb-20 md:py-28 lg:mb-28">
      <div className="mx-auto max-w-4xl px-4">
        <div>
          <h3 className="mb-6 font-sans text-3xl font-bold leading-none tracking-tight text-black sm:text-4xl">
            {title}
          </h3>
          <div className="mb-8">
            <span
              className={`inline-block h-[5px] w-[50px] bg-primary  md:h-[3px] md:w-[80px] `}
            ></span>
          </div>
          <p className="border-b-2 border-primary pb-12 text-lg">
            {description}
          </p>
          {step.map((s) => (
            <div key={s.id} className="border-b-2 border-primary py-12">
              <div className="flex flex-col gap-12 md:flex-row md:gap-52">
                <div className="flex h-16 w-16 shrink-0 items-center justify-center rounded-full border-2 border-orange text-lg font-semibold text-primary">
                  {s.stepNumber}
                </div>
                <div>
                  <h4 className="mb-6 font-sans text-2xl font-bold leading-none tracking-tight text-black sm:text-3xl">
                    {s.title}
                  </h4>
                  <p className="text-lg">{s.stepContent}</p>
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default HowItWork;
