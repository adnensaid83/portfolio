import Link from 'next/link';
import React from 'react';

const ButtonPrimary = ({ url, label }: { url: string; label: string }) => {
  return (
    <div className="flex items-center justify-center">
      <Link href={url} className="group">
        <div className="group relative flex items-center gap-1 overflow-hidden rounded-full border-2 border-orange bg-transparent px-16 py-2 text-base font-medium !leading-relaxed text-black duration-300 after:absolute after:bottom-0 after:left-0 after:right-0 after:top-0 after:z-0 after:bg-orange group-hover:text-black group-hover:after:top-full after:motion-safe:duration-300 sm:text-lg md:text-xl">
          <span className="relative z-[1] motion-safe:duration-100">
            {label}
          </span>
        </div>
      </Link>
    </div>
  );
};

export default ButtonPrimary;
