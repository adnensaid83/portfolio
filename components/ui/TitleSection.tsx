'use client';
import React from 'react';
const TitleSection = ({ title }: { title: string }) => {
  let [firstWord, ...restOfTheStringArray] = title.split(/\s+/);
  const restOfTheString = restOfTheStringArray.join(' ');

  return (
    <div className="flex flex-col items-center">
      <h2 className="mb-6 max-w-lg text-center font-sans text-3xl font-bold uppercase leading-none tracking-tight text-black sm:text-4xl md:mx-auto">
        <span className="relative inline-block text-primary">
          <svg
            viewBox="0 0 52 24"
            fill="currentColor"
            className="text-blue-gray-100 absolute left-0 top-0 z-0 -ml-20 -mt-8 hidden w-32 sm:block lg:-ml-28 lg:-mt-5 lg:w-32"
          >
            <defs>
              <pattern
                id="2feffae2-9edf-414e-ab8c-f0e6396a0fc1"
                x="0"
                y="0"
                width=".135"
                height=".30"
              >
                <circle cx="1" cy="1" r=".7" />
              </pattern>
            </defs>
            <rect
              fill="url(#2feffae2-9edf-414e-ab8c-f0e6396a0fc1)"
              width="52"
              height="24"
            />
          </svg>
          <span className="relative text-primary">{firstWord}</span>
        </span>
        {' ' + restOfTheString}
        <div className="text-center">
          <span
            className={`inline-block h-[5px] w-[50px] bg-primary  md:h-[3px] md:w-[80px] `}
          ></span>
        </div>
      </h2>
    </div>
  );
};

export default TitleSection;
