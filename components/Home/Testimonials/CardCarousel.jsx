'use client';

import { StructuredText as StructuredTextField } from 'react-datocms/structured-text';
import { Image as DatoImage } from 'react-datocms';
import { useEffect, useRef } from 'react';
import Highlighter from '@/components/Common/Highlighter';

import { Autoplay, EffectCards } from 'swiper/modules';
import { register } from 'swiper/element/bundle';
register();

const CardCarrousel = ({ reviews }) => {
  const swiperRef = useRef(null);
  const progressCircle = useRef(null);

  const progressContent = useRef(null);
  const onAutoplayTimeLeft = (s, time, progress) => {
    progressCircle.current.style.setProperty('--progress', 1 - progress);
    progressContent.current.textContent = `${Math.ceil(time / 1000)}s`;
  };
  useEffect(() => {
    const swiperContainer = swiperRef.current;
    const params = {
      navigation: true,
      effect: 'cards',
      grabCursor: true,
      modules: [EffectCards, Autoplay],
      onAutoplayTimeLeft,
      autoplay: {
        delay: 10000,
        disableOnInteraction: false,
      },
      cardsEffect: {},
      injectStyles: [
        `
        .swiper-button-next,
        .swiper-button-prev {
          display:none;
        }     
        .swiper-button-next::after,
        .swiper-button-prev::after {
          content: "";
        }
        .swiper-pagination-bullet{
          width: 16px;
          height: 16px;
          border:2px solid white;
          transform:translateY(-28px);
        }
        .swiper-pagination-bullet-active{
          background-color: #F3DE76;
        }
        @media (min-width: 768px) {
          .swiper-button-next,
          .swiper-button-prev {
            display:block;
            top:auto;
            bottom:60px;
            color:white;
            width:16px;
          }
          .swiper-button-prev{
            left:90vw;
            }
          .swiper-button-next {
            margin-right:2%;
          }
          .swiper-pagination {
              text-align:start;
              margin-left:2vw;
            }
          .swiper-pagination-bullet{
            width: 20px;
            height: 20px;
            transform:translateY(-60px);
          }
            
        }
        @media (min-width: 992px) {
          .swiper-button-prev{
            left:85vw;
            }
          .swiper-button-next {
            margin-right:2%;
          }
          .swiper-pagination {
              text-align:start;
              margin-left:2vw;
            }   
        }
        @media (min-width: 1200px) {
          .swiper-button-prev{
            left:88vw;
            }
          .swiper-button-next {
            margin-right:4%;
          }
          .swiper-pagination {
              text-align:start;
              margin-left:4%;
            }
        }
      @media (min-width: 1400px) {
          .swiper-button-prev{
            left:78vw;
            }
          .swiper-button-next {
            margin-right:15%;
          }
          .swiper-pagination {
              text-align:start;
              margin-left:15%;
            }
        }
    `,
      ],
    };
    Object.assign(swiperContainer, params);
    swiperContainer.initialize();
  }, []);

  return (
    <>
      <swiper-container
        ref={swiperRef}
        init="false"
        class="my-96 h-[420px] w-[740px]"
      >
        {reviews.map((review, index) => (
          <swiper-slide className="" key={review.id}>
            <SingleReview testimonial={review} />
          </swiper-slide>
        ))}
        <div
          className="absolute bottom-[16px] right-[16px] z-10 flex h-[48px] w-[48px] items-center justify-center text-blue-50"
          slot="container-end"
        >
          <svg
            viewBox="0 0 48 48"
            ref={progressCircle}
            className="absolute left-0 top-0 z-10 h-full w-full rotate-[-90deg] transform"
            style={{
              strokeWidth: 4,
              strokeDasharray: 125.6,
              strokeDashoffset: `calc(125.6px * (1 - var(--progress)))`,
              stroke: 'blue',
              fill: 'none',
            }}
          >
            <circle cx="24" cy="24" r="20"></circle>
          </svg>
          <span ref={progressContent}></span>
        </div>
      </swiper-container>
    </>
  );
};

export default CardCarrousel;

const starIcon = (
  <svg width="18" height="16" viewBox="0 0 18 16" className="fill-current">
    <path d="M9.09815 0.361679L11.1054 6.06601H17.601L12.3459 9.59149L14.3532 15.2958L9.09815 11.7703L3.84309 15.2958L5.85035 9.59149L0.595291 6.06601H7.0909L9.09815 0.361679Z" />
  </svg>
);

const SingleReview = ({ testimonial }) => {
  const { rating, reviewerName, reviewerPicture, review, reviewerTitle } =
    testimonial;

  let ratingIcons = [];
  for (let index = 0; index < rating; index++) {
    ratingIcons.push(
      <span key={index} className="text-yellow">
        {starIcon}
      </span>
    );
  }

  return (
    <div className="h-96 w-full bg-green last:block md:last:hidden lg:last:block">
      <div className="mx-auto flex h-full flex-col items-center justify-center rounded-md p-8 shadow-one dark:bg-[#1D2144] lg:items-start lg:px-5 xl:px-8">
        <div className="mb-5 flex items-center space-x-1">{ratingIcons}</div>
        <div className="mb-8 h-36 border-b border-body-color border-opacity-10 pb-8 text-base leading-relaxed text-black dark:border-white dark:border-opacity-10 dark:text-white">
          <StructuredTextField data={review.value} renderNode={Highlighter} />
        </div>
        <div className="flex items-center px-12 md:w-full md:px-4 lg:px-0">
          <div className="relative mr-4 h-[50px] w-full max-w-[50px] overflow-hidden rounded-full">
            <DatoImage
              data={reviewerPicture.responsiveImage}
              className="h-full w-full object-contain"
              layout="fill"
              objectFit="cover"
              objectPosition="50% 50%"
            />
          </div>
          <div className="w-full">
            <p className="mb-1 text-lg font-semibold text-black dark:text-white lg:text-base xl:text-lg">
              {reviewerName}
            </p>
            <p className="text-sm text-black">{reviewerTitle}</p>
          </div>
        </div>
      </div>
    </div>
  );
};
