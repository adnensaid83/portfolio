import ButtonPrimary from '@/components/ui/ButtonPrimary';
import TitleSection from '@/components/ui/TitleSection';
import { ButtonRecord } from '@/graphql/types/graphql';
import { Maybe } from 'graphql/jsutils/Maybe';
import React from 'react';

type Props = {
  title: string;
  subtitle: Maybe<string> | undefined;
  button: ButtonRecord;
};

const Contact = ({ title, subtitle, button }: Props) => {
  return (
    <section className="bg-green px-4 py-12 md:px-24 lg:px-8 lg:py-20">
      <TitleSection title={title} />
      <p className="mx-auto max-w-xl py-12 text-center text-2xl md:text-3xl lg:text-4xl">
        {subtitle}
      </p>
      <ButtonPrimary label={button.label} url={button.url || '#'} />
    </section>
  );
};

export default Contact;
