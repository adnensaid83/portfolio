import { ServiceRecord } from '@/graphql/types/graphql';
import Link from 'next/link';
import React from 'react';
import ReactMarkdown from 'react-markdown';

type Props = {
  service: ServiceRecord;
  locale: string;
};
const ServiceItem = ({ service, locale }: Props) => {
  const { tag, title, description, pointsTo } = service;
  const ariaLabel = locale === 'ar' ? 'Flèche gauche' : 'Flèche droite';
  return (
    <Link
      href={pointsTo.slug}
      className={` border-[3px] border-primary even:border-b-0 even:border-t-0 lg:border-b-0 ${
        locale === 'ar' ? 'lg:even:border-r-0' : 'lg:even:border-l-0'
      } lg:even:border-t-[3px] `}
    >
      <div className="group/item hover:scale-101 flex min-h-[450px] cursor-pointer flex-col gap-8 px-12 py-12 transition duration-150 ease-out hover:bg-primary hover:text-white hover:ease-in">
        <p className="text-base font-light uppercase text-primary group-hover/item:text-white">
          {tag}
        </p>
        <h2 className="md:text-4.4xl text-3xl font-normal text-black group-hover/item:text-white md:max-w-[30rem]">
          {title}
        </h2>
        <div className="group-hover/item:p:text-white prose prose-xl line-clamp-4 text-xl font-light prose-p:text-xl prose-p:leading-normal prose-p:text-black group-hover/item:prose-p:text-white">
          <ReactMarkdown>{description}</ReactMarkdown>
        </div>
        <button
          id={title}
          aria-label={ariaLabel}
          className="origin-top-center h-10 w-10 text-primary group-hover/item:animate-bounce-x group-hover/item:text-white"
        >
          {locale === 'ar' ? <ArrowLeft /> : <Arrowright />}
        </button>
      </div>
    </Link>
  );
};

export default ServiceItem;

const ArrowLeft = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="100%"
    height="100%"
    fill="currentColor"
    className="bi bi-arrow-left"
    viewBox="0 0 16 16"
  >
    <path
      fillRule="evenodd"
      d="M15 8a.5.5 0 0 0-.5-.5H2.707l3.147-3.146a.5.5 0 1 0-.708-.708l-4 4a.5.5 0 0 0 0 .708l4 4a.5.5 0 0 0 .708-.708L2.707 8.5H14.5A.5.5 0 0 0 15 8"
    />
  </svg>
);

const Arrowright = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="100%"
    height="100%"
    fill="currentColor"
    className="bi bi-arrow-right"
    viewBox="0 0 16 16"
  >
    <path
      fillRule="evenodd"
      d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8"
    />
  </svg>
);
