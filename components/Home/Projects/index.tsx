import { RegularList } from '@/components/Common/RegularList';
import TitleSection from '@/components/ui/TitleSection';
import { ButtonRecord, ProjectRecord } from '@/graphql/types/graphql';
import React from 'react';
import ProjectItem from './ProjectItem';
import ButtonPrimary from '@/components/ui/ButtonPrimary';

type Props = {
  title: string;
  project: ProjectRecord[];
  button: any;
};
const Project = ({ title, project, button }: Props) => {
  return (
    <section className="bg-green py-12 lg:py-20">
      <TitleSection title={title} />
      <div className="mx-auto max-w-[1200px] px-6">
        <ul className="grid grid-cols-1 place-items-center gap-6 py-12 md:grid-cols-2 md:gap-16 lg:grid-cols-3 lg:gap-16 ">
          <RegularList
            items={project}
            resourceName="project"
            itemComponent={ProjectItem}
          />
        </ul>
        {button && (
          <ButtonPrimary url={button.url || '#'} label={button.label} />
        )}
      </div>
    </section>
  );
};

export default Project;
