'use client';
import React, { useState } from 'react';
import { Maybe } from 'graphql/jsutils/Maybe';
import { ButtonRecord, FileField, SkillRecord } from '@/graphql/types/graphql';
import { primaryColor } from '@/app/i18n/settings';
import { useScroll, useTransform, motion } from 'framer-motion';
import Link from 'next/link';
import { ReactMarkdown } from 'react-markdown/lib/react-markdown';
import { RegularList } from '@/components/Common/RegularList';
import { Image as DatoImage } from 'react-datocms';
import Image from 'next/image';

type Props = {
  heroTitle: string;
  heroSubtitle: Maybe<string>;
  buttons: ButtonRecord[];
  image: Maybe<FileField> | undefined;
  skillTitle: Maybe<string>;
  skill: SkillRecord[];
  locale?: string;
};

const PortraitImage = ({
  heroTitle,
  heroSubtitle,
  buttons,
  image,
  skillTitle,
  skill,
  locale,
}: Props) => {
  const { scrollYProgress } = useScroll();
  const y = useTransform(scrollYProgress, [0, 1], ['0%', '500%']);
  return (
    <>
      <div className="relative z-10 overflow-hidden bg-green pb-16 pt-20">
        <div className="container">
          <div className="-mx-4 flex flex-wrap">
            <div className="w-full px-4">
              <div className="mx-auto max-w-[930px] p-[1rem]">
                <div className="flex flex-col-reverse items-center gap-8 md:flex-row lg:gap-40">
                  <div>
                    <motion.div
                      initial={{ opacity: 0 }}
                      animate={{ opacity: 1 }}
                      transition={{ duration: 0.5, delay: 0.4 }}
                    >
                      <h1 className="mb-5 text-center text-3xl font-extrabold text-primary dark:text-white sm:text-4xl  md:text-start md:text-5xl">
                        {heroTitle}
                      </h1>
                      <div className="mb-8 text-center text-base font-medium !leading-relaxed text-black dark:text-white dark:opacity-90 sm:text-lg md:text-start md:text-xl">
                        <ReactMarkdown>{heroSubtitle || ''}</ReactMarkdown>
                      </div>
                    </motion.div>
                    <motion.div
                      initial={{ opacity: 0 }}
                      animate={{ opacity: 1 }}
                      transition={{ duration: 0.6, delay: 0.8 }}
                      className="flex flex-col items-center space-y-4 sm:flex-row sm:space-x-4 sm:space-y-0"
                    >
                      {buttons.map((button) => {
                        const primary =
                          'bg-primary text-white hover:bg-primary';
                        const secondary =
                          'bg-black/20 hover:bg-black/30 text-black';
                        return (
                          <div className="group" key={button.id}>
                            <Link
                              href={button.url || '#'}
                              style={{
                                backgroundColor: button.primary
                                  ? primary
                                  : secondary,
                              }}
                              className={
                                '' + (button.primary ? primary : secondary)
                              }
                              id={button.id}
                            >
                              <div className="group relative flex items-center gap-1 overflow-hidden rounded-full border-2 border-orange bg-transparent px-16 py-2 text-base font-medium !leading-relaxed text-black duration-300 after:absolute after:bottom-0 after:left-0 after:right-0 after:top-0 after:z-0 after:bg-orange group-hover:text-black group-hover:after:top-full after:motion-safe:duration-300 sm:text-lg md:text-xl">
                                <span className="relative z-[1] motion-safe:duration-100">
                                  {button.label}
                                </span>
                              </div>
                            </Link>
                          </div>
                        );
                      })}
                    </motion.div>
                  </div>
                  <motion.div
                    initial={{ opacity: 0 }}
                    animate={{ opacity: 1 }}
                    transition={{ duration: 0.5, delay: 0.8 }}
                  >
                    <div className=" flex h-[320px] w-[320px] animate-morphAnimation items-center justify-center overflow-hidden rounded-full border-4 border-primary lg:h-[350px] lg:w-[350px]">
                      {image && image.responsiveImage && (
                        <DatoImage
                          data={image?.responsiveImage}
                          className="h-full w-full object-contain"
                          objectFit="cover"
                          objectPosition="50% 50%"
                        />
                      )}
                    </div>
                  </motion.div>
                </div>
                <TechStack title={skillTitle} data={skill} />
              </div>
            </div>
          </div>
        </div>
        <motion.div
          initial={{ opacity: 0 }}
          animate={{ opacity: 1 }}
          transition={{ duration: 0.5, delay: 0.1 }}
          style={{ y }}
          className={`absolute ${
            locale === 'ar' ? 'left-0' : 'right-0'
          }  top-0 z-[-1] opacity-30 lg:opacity-100`}
        >
          <svg
            width="450"
            height="556"
            viewBox="0 0 450 556"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            className={`rotate-90 md:rotate-0 ${
              locale === 'ar' && 'lg:-rotate-90'
            }`}
          >
            <circle
              cx="277"
              cy="63"
              r="225"
              fill="url(#paint0_linear_25:217)"
            />
            <circle
              cx="17.9997"
              cy="182"
              r="18"
              fill="url(#paint1_radial_25:217)"
            />
            <circle
              cx="76.9997"
              cy="288"
              r="34"
              fill="url(#paint2_radial_25:217)"
            />
            <circle
              cx="325.486"
              cy="302.87"
              r="180"
              transform="rotate(-37.6852 325.486 302.87)"
              fill="url(#paint3_linear_25:217)"
            />
            <circle
              opacity="0.8"
              cx="184.521"
              cy="315.521"
              r="132.862"
              transform="rotate(114.874 184.521 315.521)"
              stroke="url(#paint4_linear_25:217)"
            />
            <circle
              opacity="0.8"
              cx="356"
              cy="290"
              r="179.5"
              transform="rotate(-30 356 290)"
              stroke="url(#paint5_linear_25:217)"
            />
            <circle
              opacity="0.8"
              cx="191.659"
              cy="302.659"
              r="133.362"
              transform="rotate(133.319 191.659 302.659)"
              fill="url(#paint6_linear_25:217)"
            />
            <defs>
              <linearGradient
                id="paint0_linear_25:217"
                x1="-54.5003"
                y1="-178"
                x2="222"
                y2="288"
                gradientUnits="userSpaceOnUse"
              >
                <stop stopColor={primaryColor} />
                <stop offset="1" stopColor={primaryColor} stopOpacity="0" />
              </linearGradient>
              <radialGradient
                id="paint1_radial_25:217"
                cx="0"
                cy="0"
                r="1"
                gradientUnits="userSpaceOnUse"
                gradientTransform="translate(17.9997 182) rotate(90) scale(18)"
              >
                <stop
                  offset="0.145833"
                  stopColor={primaryColor}
                  stopOpacity="0"
                />
                <stop offset="1" stopColor={primaryColor} stopOpacity="0.08" />
              </radialGradient>
              <radialGradient
                id="paint2_radial_25:217"
                cx="0"
                cy="0"
                r="1"
                gradientUnits="userSpaceOnUse"
                gradientTransform="translate(76.9997 288) rotate(90) scale(34)"
              >
                <stop
                  offset="0.145833"
                  stopColor={primaryColor}
                  stopOpacity="0"
                />
                <stop offset="1" stopColor={primaryColor} stopOpacity="0.08" />
              </radialGradient>
              <linearGradient
                id="paint3_linear_25:217"
                x1="226.775"
                y1="-66.1548"
                x2="292.157"
                y2="351.421"
                gradientUnits="userSpaceOnUse"
              >
                <stop stopColor={primaryColor} />
                <stop offset="1" stopColor={primaryColor} stopOpacity="0" />
              </linearGradient>
              <linearGradient
                id="paint4_linear_25:217"
                x1="184.521"
                y1="182.159"
                x2="184.521"
                y2="448.882"
                gradientUnits="userSpaceOnUse"
              >
                <stop stopColor={primaryColor} />
                <stop offset="1" stopColor="white" stopOpacity="0" />
              </linearGradient>
              <linearGradient
                id="paint5_linear_25:217"
                x1="356"
                y1="110"
                x2="356"
                y2="470"
                gradientUnits="userSpaceOnUse"
              >
                <stop stopColor={primaryColor} />
                <stop offset="1" stopColor="white" stopOpacity="0" />
              </linearGradient>
              <linearGradient
                id="paint6_linear_25:217"
                x1="118.524"
                y1="29.2497"
                x2="166.965"
                y2="338.63"
                gradientUnits="userSpaceOnUse"
              >
                <stop stopColor={primaryColor} />
                <stop offset="1" stopColor={primaryColor} stopOpacity="0" />
              </linearGradient>
            </defs>
          </svg>
        </motion.div>
        <motion.div
          style={{ y }}
          initial={{ opacity: 0 }}
          animate={{ opacity: 1 }}
          transition={{ duration: 0.5, delay: 0.1 }}
          className={`absolute bottom-0 ${
            locale === 'ar' ? 'right-0' : 'left-0'
          } z-[-1] opacity-30 lg:opacity-100`}
        >
          <svg
            width="364"
            height="201"
            viewBox="0 0 364 201"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              d="M5.88928 72.3303C33.6599 66.4798 101.397 64.9086 150.178 105.427C211.155 156.076 229.59 162.093 264.333 166.607C299.076 171.12 337.718 183.657 362.889 212.24"
              stroke="url(#paint0_linear_25:218)"
            />
            <path
              d="M-22.1107 72.3303C5.65989 66.4798 73.3965 64.9086 122.178 105.427C183.155 156.076 201.59 162.093 236.333 166.607C271.076 171.12 309.718 183.657 334.889 212.24"
              stroke="url(#paint1_linear_25:218)"
            />
            <path
              d="M-53.1107 72.3303C-25.3401 66.4798 42.3965 64.9086 91.1783 105.427C152.155 156.076 170.59 162.093 205.333 166.607C240.076 171.12 278.718 183.657 303.889 212.24"
              stroke="url(#paint2_linear_25:218)"
            />
            <path
              d="M-98.1618 65.0889C-68.1416 60.0601 4.73364 60.4882 56.0734 102.431C120.248 154.86 139.905 161.419 177.137 166.956C214.37 172.493 255.575 186.165 281.856 215.481"
              stroke="url(#paint3_linear_25:218)"
            />
            <circle
              opacity="0.8"
              cx="214.505"
              cy="60.5054"
              r="49.7205"
              transform="rotate(-13.421 214.505 60.5054)"
              stroke="url(#paint4_linear_25:218)"
            />
            <circle cx="220" cy="63" r="43" fill="url(#paint5_radial_25:218)" />
            <defs>
              <linearGradient
                id="paint0_linear_25:218"
                x1="184.389"
                y1="69.2405"
                x2="184.389"
                y2="212.24"
                gradientUnits="userSpaceOnUse"
              >
                <stop stopColor={primaryColor} stopOpacity="0" />
                <stop offset="1" stopColor={primaryColor} />
              </linearGradient>
              <linearGradient
                id="paint1_linear_25:218"
                x1="156.389"
                y1="69.2405"
                x2="156.389"
                y2="212.24"
                gradientUnits="userSpaceOnUse"
              >
                <stop stopColor={primaryColor} stopOpacity="0" />
                <stop offset="1" stopColor={primaryColor} />
              </linearGradient>
              <linearGradient
                id="paint2_linear_25:218"
                x1="125.389"
                y1="69.2405"
                x2="125.389"
                y2="212.24"
                gradientUnits="userSpaceOnUse"
              >
                <stop stopColor={primaryColor} stopOpacity="0" />
                <stop offset="1" stopColor={primaryColor} />
              </linearGradient>
              <linearGradient
                id="paint3_linear_25:218"
                x1="93.8507"
                y1="67.2674"
                x2="89.9278"
                y2="210.214"
                gradientUnits="userSpaceOnUse"
              >
                <stop stopColor={primaryColor} stopOpacity="0" />
                <stop offset="1" stopColor={primaryColor} />
              </linearGradient>
              <linearGradient
                id="paint4_linear_25:218"
                x1="214.505"
                y1="10.2849"
                x2="212.684"
                y2="99.5816"
                gradientUnits="userSpaceOnUse"
              >
                <stop stopColor={primaryColor} />
                <stop offset="1" stopColor={primaryColor} stopOpacity="0" />
              </linearGradient>
              <radialGradient
                id="paint5_radial_25:218"
                cx="0"
                cy="0"
                r="1"
                gradientUnits="userSpaceOnUse"
                gradientTransform="translate(220 63) rotate(90) scale(43)"
              >
                <stop offset="0.145833" stopColor="white" stopOpacity="0" />
                <stop offset="1" stopColor="white" stopOpacity="0.08" />
              </radialGradient>
            </defs>
          </svg>
        </motion.div>
        {/*         <div className="absolute left-80 top-36 z-[-1] animate-scale-up-center-delay">
          <Image
            src={'/star-green.png'}
            alt="star white"
            width={100}
            height={100}
          />
        </div>
        <div className="absolute bottom-20 left-96 z-[-1] animate-scale-up-center">
          <Image
            src={'/star-yellow.png'}
            alt="star white"
            width={100}
            height={100}
          />
        </div>
        <div className="absolute bottom-72 right-80 z-[-1] animate-scale-up-center-delay">
          <Image
            src={'/star-yellow.png'}
            alt="star yellow"
            width={80}
            height={80}
          />
        </div>
        <div className="absolute bottom-20 right-80 z-[-1] animate-scale-up-center">
          <Image
            src={'/star-green.png'}
            alt="star green"
            width={150}
            height={150}
          />
        </div> */}
      </div>
    </>
  );
};

export default PortraitImage;

function TechStack({
  title,
  data,
}: {
  title?: Maybe<string>;
  data: SkillRecord[];
}) {
  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      transition={{ duration: 0.6, delay: 0.8 }}
      className="pt-8 md:pt-12 lg:flex lg:items-center lg:justify-between"
    >
      <p className="mb-8 text-center text-base font-medium !leading-relaxed text-black dark:text-white dark:opacity-90 sm:text-lg md:text-xl lg:mb-0 lg:text-start">
        {title}
      </p>
      <span className="hidden lg:block lg:h-[30px] lg:w-[3px] lg:bg-primary"></span>
      <ul className="relative flex flex-wrap justify-center gap-2 md:gap-4 md:px-12 lg:px-0 lg:pl-0">
        <RegularList
          items={data}
          resourceName="skill"
          itemComponent={TechStackItem}
        />
      </ul>
    </motion.div>
  );
}
function TechStackItem({ skill }: { skill: { label: string; url: string } }) {
  const [isShowing, setIsShowing] = useState(false);
  const { url, label } = skill;

  return (
    <li className="">
      <button
        onMouseEnter={() => setIsShowing(true)}
        onMouseLeave={() => setIsShowing(false)}
      >
        <Image src={url} alt={label} width={44} height={44} priority />
      </button>
      {isShowing ? (
        <p className="absolute z-10 rounded-xl bg-black px-4 py-1 text-xs font-bold uppercase text-white">
          {label}
        </p>
      ) : null}
    </li>
  );
}
