import 'client-only';

export function getLocalStorage(key: string, defaultValue: any) {
  try {
    const stickyValue = localStorage.getItem(key);
    if (stickyValue === null || stickyValue === 'undefined') {
      return defaultValue;
    }
    return JSON.parse(stickyValue);
  } catch (error) {
    console.error('Error parsing localStorage value:', error);
    return defaultValue;
  }
}

export function setLocalStorage(key: string, value: any) {
  try {
    localStorage.setItem(key, JSON.stringify(value));
  } catch (error) {
    console.error('Error setting localStorage value:', error);
  }
}
