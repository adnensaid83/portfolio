export function isArabic(lng: string) {
  return lng === 'ar';
}
