import Footer from '@/components/Footer';
import 'node_modules/react-modal-video/css/modal-video.css';
import '@/styles/global.css';
import { SiteLocale } from '@/graphql/types/graphql';
import getAvailableLocales from '@/app/i18n/settings';
import HeaderRenderer from '@/components/Header/HeaderRenderer';
import { isArabic } from '@/utils/isArabic';
import { draftMode } from 'next/headers';

type Params = {
  children: React.ReactNode;
  params: {
    lng: SiteLocale;
  };
};

export async function generateStaticParams() {
  const languages = await getAvailableLocales();
  return languages.map((language) => {
    language;
  });
}
export default async function RootLayout({
  children,
  params: { lng },
}: Params) {
  //const { isEnabled } = draftMode();
  const isEnabled = true;
  const direction = isArabic(lng) ? 'rtl' : 'ltr';
  return (
    <div dir={direction}>
      <HeaderRenderer lng={lng} isDraft={isEnabled} />
      {children}
      <Footer lng={lng} />
    </div>
  );
}
