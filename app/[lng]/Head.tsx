'use client';

import { usePathname } from 'next/navigation';

type PageInfo = {
  title: string;
  description: string;
};
export default function Head({ lng }: { lng: string }) {
  const pathname = usePathname();
  const getPageInfo = (): PageInfo => {
    switch (pathname) {
      case '/fr/home':
        return {
          title:
            'Accueil - Adnen Said - Développeur web passionné située en région lyonnaise',
          description:
            "Bienvenue sur le site d'Adnen Said, développeur web passionné basé à Lyon. Spécialisé dans le développement de sites web, la conversion de maquettes en sites web, la refonte de sites et le référencement SEO. Offrez-vous des sites performants, 100% optimisés selon les normes Lighthouse. En plus, découvrez nos services de conception de flyers, logos, cartes de visite et invitations de mariage. Contactez-nous dès maintenant pour donner vie à vos projets web !",
        };
      case '/fr/digital-product-creation':
        return {
          title: 'Services - Création de produits digitaux',
          description:
            'Découvrez nos services de création de produits digitaux sur mesure. Chez Adnen Said, nous transformons vos idées en réalité numérique. Spécialisés dans le développement de sites web, la conversion de maquettes en sites web, la refonte de sites et le référencement SEO, nous vous offrons des solutions personnalisées pour répondre à vos besoins spécifiques. Obtenez des produits digitaux de haute qualité, 100% optimisés selon les normes Lighthouse. Explorez également nos services de conception de flyers, logos, cartes de visite et invitations de mariage. Contactez-nous dès maintenant pour créer ensemble votre prochain projet digital !',
        };
      case '/fr/conversion-of-mockup-into-website':
        return {
          title: 'Services - Conversion maquettes en site web',
          description:
            'Découvrez nos services de conversion de maquettes en sites web. Chez Adnen Said, nous transformons vos designs créatifs en sites web interactifs et fonctionnels. Avec une expertise solide dans le développement web, nous assurons une conversion précise et fidèle de vos maquettes en sites web optimisés pour une expérience utilisateur exceptionnelle. Obtenez des sites web performants, 100% optimisés selon les normes Lighthouse, et découvrez également nos services de refonte de sites et de référencement SEO. Contactez-nous dès maintenant pour concrétiser votre vision en ligne !',
        };
      case '/fr/portfolio':
        return {
          title:
            'Portfolio - Jetez un œil à certains des derniers projets créés pour mes clients.',
          description:
            "Explorez notre portfolio et découvrez certains des derniers projets que nous avons réalisés pour nos clients. Chez Adnen Said, nous sommes fiers de présenter notre travail, allant de la création de sites web uniques à la conception de logos, en passant par la réalisation de flyers et d'invitations de mariage. Chaque projet est le résultat d'une collaboration étroite avec nos clients pour répondre à leurs besoins et atteindre leurs objectifs. Parcourez notre portfolio pour trouver l'inspiration et imaginez ce que nous pouvons réaliser ensemble pour votre prochain projet digital. Contactez-nous dès maintenant pour discuter de vos idées !",
        };
      case '/fr/contact':
        return {
          title: 'Contact - Raconte-moi ton idée !',
          description:
            "Vous avez une idée ou un projet en tête ? Nous sommes là pour vous aider à le concrétiser ! Contactez-nous dès maintenant pour discuter de votre projet. Chez Adnen Said, nous sommes spécialisés dans la création de sites web sur mesure, la conception de logos, la réalisation de flyers et bien plus encore. Nous sommes impatients d'entendre parler de votre idée et de collaborer avec vous pour la transformer en réalité digitale. Remplissez le formulaire de contact ci-dessous et nous vous recontacterons dans les plus brefs délais. Laissez-nous vous aider à faire de votre vision une réussite en ligne !",
        };
      case '/en/home':
        return {
          title:
            'Accueil - Adnen Said - Passionate Web Developer based in Lyon',
          description:
            "Welcome to Adnen Said's website, a passionate web developer based in Lyon. Specialized in website development, mockup conversion, website redesign, and SEO. Get high-performance websites, 100% optimized according to Lighthouse standards. Also, discover our services for designing flyers, logos, business cards, and wedding invitations. Contact us now to bring your web projects to life!",
        };
      case '/en/digital-product-creation':
        return {
          title: 'Services - Digital Product Creation',
          description:
            'Discover our custom digital product creation services. At Adnen Said, we turn your ideas into digital reality. Specialized in website development, mockup conversion, website redesign, and SEO, we offer personalized solutions to meet your specific needs. Get high-quality digital products, 100% optimized according to Lighthouse standards. Explore also our services for designing flyers, logos, business cards, and wedding invitations. Contact us now to create your next digital project together!',
        };
      case '/en/conversion-of-mockup-into-website':
        return {
          title: 'Services - Mockup to Website Conversion',
          description:
            'Explore our mockup to website conversion services. At Adnen Said, we turn your creative designs into interactive and functional websites. With a solid expertise in web development, we ensure precise and faithful conversion of your mockups into websites optimized for an exceptional user experience. Get high-performance websites, 100% optimized according to Lighthouse standards. Also, discover our services for website redesign and SEO. Contact us now to bring your vision online!',
        };
      case '/en/portfolio':
        return {
          title:
            'Portfolio - Check out some of the latest projects created for my clients.',
          description:
            'Explore our portfolio and discover some of the latest projects we have created for our clients. At Adnen Said, we take pride in showcasing our work, ranging from unique website creations to logo designs, flyers, and wedding invitations. Each project is the result of close collaboration with our clients to meet their needs and achieve their goals. Browse our portfolio for inspiration and imagine what we can achieve together for your next digital project. Contact us now to discuss your ideas!',
        };
      case '/en/contact':
        return {
          title: 'Contact - Tell me about your idea!',
          description:
            "Do you have an idea or project in mind? We're here to help you bring it to life! Contact us now to discuss your project. At Adnen Said, we specialize in custom website creation, logo design, flyer creation, and much more. We're eager to hear about your idea and collaborate with you to turn it into digital reality. Fill out the contact form below and we'll get back to you as soon as possible. Let us help you make your vision a success online!",
        };
      case '/ar/home':
        return {
          title: 'الرئيسية - عدنان سعيد -   مهندس برمجيات مقره في ليون',
          description:
            'مرحبًا بك في موقع عدنان سعيد، مطور ويب متحمس مقره في ليون. متخصصون في تطوير مواقع الويب، تحويل النماذج إلى مواقع الويب، إعادة تصميم المواقع، وتحسين محركات البحث. احصل على مواقع ويب عالية الأداء، مُحسَّنة بنسبة 100٪ وفقًا لمعايير Lighthouse. اكتشف أيضًا خدماتنا لتصميم النشرات الإعلانية، والشعارات، وبطاقات العمل، ودعوات الزفاف. اتصل بنا الآن لإحياء مشاريعك على الويب!',
        };
      case '/ar/digital-product-creation':
        return {
          title: 'الخدمات - إنشاء منتجات رقمية',
          description:
            'اكتشف خدماتنا المخصصة لإنشاء المنتجات الرقمية. في عدنان سعيد، نحول أفكارك إلى واقع رقمي. متخصصون في تطوير مواقع الويب، تحويل النماذج إلى مواقع الويب، إعادة تصميم المواقع، وتحسين محركات البحث، نقدم حلولًا شخصية لتلبية احتياجاتك الخاصة. احصل على منتجات رقمية عالية الجودة، مُحسَّنة بنسبة 100٪ وفقًا لمعايير Lighthouse. اكتشف أيضًا خدماتنا لتصميم النشرات الإعلانية، والشعارات، وبطاقات العمل، ودعوات الزفاف. اتصل بنا الآن لإنشاء مشروعك الرقمي التالي معًا!',
        };
      case '/ar/conversion-of-mockup-into-website':
        return {
          title: 'الخدمات - تحويل النماذج إلى موقع ويب',
          description:
            'استكشف خدماتنا لتحويل النماذج إلى مواقع الويب. في عدنان سعيد، نحول تصميماتك الإبداعية إلى مواقع ويب تفاعلية ووظيفية. بفضل خبرتنا الصلبة في تطوير الويب، نضمن تحويلًا دقيقًا ومخلصًا لنماذجك إلى مواقع ويب مُحسَّنة لتجربة مستخدم استثنائية. احصل على مواقع ويب عالية الأداء، مُحسَّنة بنسبة 100٪ وفقًا لمعايير Lighthouse. اكتشف أيضًا خدماتنا لإعادة تصميم المواقع وتحسين محركات البحث. اتصل بنا الآن لإحياء رؤيتك على الإنترنت!',
        };
      case '/ar/portfolio':
        return {
          title: 'المعرض - تصفح بعض أحدث المشاريع التي تم إنشاؤها لعملائي.',
          description:
            'استكشف معرضنا واكتشف بعض أحدث المشاريع التي قمنا بإنشائها لعملائنا. في عدنان سعيد، نفتخر بعرض أعمالنا، بدءًا من إنشاء مواقع ويب فريدة إلى تصميم الشعارات والنشرات الإعلانية ودعوات الزفاف. كل مشروع هو نتيجة لتعاون وثيق مع عملائنا لتلبية احتياجاتهم وتحقيق أهدافهم. تصفح معرضنا للحصول على الإلهام وتخيل ما يمكننا تحقيقه معًا لمشروعك الرقمي التالي. اتصل بنا الآن لمناقشة أفكارك!',
        };
      case '/ar/contact':
        return {
          title: 'الاتصال - أخبرني عن فكرتك!',
          description:
            'هل لديك فكرة أو مشروع في الاعتبار؟ نحن هنا لمساعدتك في تحقيقه! اتصل بنا الآن لمناقشة مشروعك. في عدنان سعيد',
        };
      default:
        return {
          title:
            'Accueil - Adnen Said - Développeur web passionné située en région lyonnaise',
          description:
            "Bienvenue sur le site d'Adnen Said, développeur web passionné basé à Lyon. Spécialisé dans le développement de sites web, la conversion de maquettes en sites web, la refonte de sites et le référencement SEO. Offrez-vous des sites performants, 100% optimisés selon les normes Lighthouse. En plus, découvrez nos services de conception de flyers, logos, cartes de visite et invitations de mariage. Contactez-nous dès maintenant pour donner vie à vos projets web !",
        };
    }
  };
  const pageInfo = getPageInfo();
  return (
    <>
      <title>{pageInfo.title}</title>
      <meta content="width=device-width, initial-scale=1" name="viewport" />
      <meta name="description" content={pageInfo.description} />
      <link rel="icon" href="/images/favicon.ico" />
      <meta property="og:image" content="/images/favicon.png" />
      {/* DNS Prefetch pour Google Fonts */}
      <link rel="dns-prefetch" href="//fonts.googleapis.com" />
      <link rel="dns-prefetch" href="//fonts.gstatic.com" />
      {/* Preconnect pour Google Fonts */}
      <link rel="preconnect" href="//fonts.googleapis.com" />
      <link
        rel="preconnect"
        href="//fonts.gstatic.com"
        crossOrigin="anonymous"
      />
      {/* Preconnect pour DatoCMS */}
      <link
        rel="preconnect"
        href="https://www.datocms-assets.com"
        crossOrigin="anonymous"
      />
    </>
  );
}

/* 
<!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-1KW30FGEM4"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-1KW30FGEM4');
</script>

*/
