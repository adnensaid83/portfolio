/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './app/**/*.{js,ts,jsx,tsx}',
    './pages/**/*.{js,ts,jsx,tsx}',
    './components/**/*.{js,ts,jsx,tsx}',
  ],
  darkMode: 'class',
  theme: {
    container: {
      center: true,
      padding: '1rem',
    },
    screens: {
      xs: '450px',
      // => @media (min-width: 450px) { ... }

      sm: '575px',
      // => @media (min-width: 576px) { ... }

      md: '768px',
      // => @media (min-width: 768px) { ... }

      lg: '992px',
      // => @media (min-width: 992px) { ... }

      xl: '1200px',
      // => @media (min-width: 1200px) { ... }

      '2xl': '1400px',
      // => @media (min-width: 1400px) { ... }
    },
    extend: {
      colors: {
        current: 'currentColor',
        transparent: 'transparent',
        white: '#FFFFFF',
        black: '#1d1d1d',
        dark: '#1D2144',
        primary: 'rgb(var(--color-primary) / <alpha-value>)',
        yellow: '#FBB040',
        green: '#dcf1e7',
        orange: '#facc15',
        'body-color': '#959CB1',
      },
      backgroundImage: {
        wave: "url('/wave-1.svg')",
      },
      boxShadow: {
        signUp: '0px 5px 10px rgba(4, 10, 34, 0.2)',
        one: '0px 2px 3px rgba(7, 7, 77, 0.05)',
        sticky: 'inset 0 -1px 0 0 rgba(0, 0, 0, 0.1)',
      },
      keyframes: {
        morph: {
          '0%': {
            borderRadius: '60% 40% 30% 70% / 60% 30% 70% 40%',
          },
          '50%': {
            borderRadius: '30% 60% 70% 40% / 50% 60% 30% 60%',
          },
          '100%': {
            borderRadius: '60% 40% 30% 70% / 60% 30% 70% 40%',
          },
        },
        bouncex: {
          '0%, 100%': {
            transform: 'translateX(0%)',
          },
          '15%': {
            transform: 'translateX(-25%) rotate(-5deg)',
          },
          '30%': {
            transform: 'translateX(20%) rotate(3deg)',
          },
          '45%': {
            transform: 'translateX(-15%) rotate(-3deg)',
          },
          '60%': {
            transform: 'translateX(10%) rotate(2deg)',
          },
          '75%': {
            transform: 'translateX(-5%) rotate(-1deg)',
          },
        },
        scaleUpCenter: {
          '0%': {
            transform: 'scale(0.5) rotate(0deg)',
            transform: 'scale(0.5) rotate(0deg)',
          },
          '100%': {
            transform: 'scale(1) rotate(15deg)',
            transform: 'scale(1) rotate(15deg)',
          },
        },
      },
      animation: {
        morphAnimation: 'morph 8s ease-in-out infinite',
        'bounce-x': 'bouncex 1s ease',
        'scale-up-center':
          'scaleUpCenter 2s cubic-bezier(0.645, 0.045, 0.355, 1.000) infinite alternate-reverse',
        'scale-up-center-delay':
          'scaleUpCenter 2s cubic-bezier(0.645, 0.045, 0.355, 1.000) 1s infinite alternate-reverse',
      },
    },
  },
  plugins: [require('@tailwindcss/typography')],
};
